from CuentaBancaria import CuentaBancaria

#instanciar objetos de CuentaBancaria
mi_cuenta = CuentaBancaria("Miguel Chacana", 1000)
mi_cuenta2 = CuentaBancaria("Daniel", 10000)
mi_cuenta3 = CuentaBancaria("Juana", 25000)

# conultar saldo
mi_cuenta.consultar_saldo()

#realizar deposito
mi_cuenta.depositar(1500)

#relizar retiro
mi_cuenta.retirar(1500)

# consultar saldo final
mi_cuenta.consultar_saldo()