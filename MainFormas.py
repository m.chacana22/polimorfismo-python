#import clases hija
from Circulo import Circulo
from Rectangulo import Rectangulo
from Triangulo import Triangulo

#definir funcion calcula area total
def area_total(formas):
    total = 0
    for forma in formas:
        total += forma.calcular_area()
    return total


#crear objetos
rectangulo = Rectangulo(5, 10)
circulo = Circulo(3)
triangulo = Triangulo(4, 6)


#utilizar area total
formas = [rectangulo, circulo, triangulo]
total_area = area_total(formas)



#imprimir resultado
print(f"El area total es: {total_area}")